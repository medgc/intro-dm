df <- data.frame(1:20, 
                 c(rep("m", 6), rep("f", 4), rep("m", 4), rep("f", 6)),
                 c("familiar", rep("deportivo", 8), "4x4", rep("familiar", 4), rep("4x4", 6)), 
                 c("s", "m", "m", "l", "xl", "xl", "s", "s", "m", "l", "l", "xl", "m", "xl", "s", "s", "m", "m", "m", "l"), 
                 c(rep(0, 10), rep(1, 10)));
colnames(df) <- c("id", "genero", "tipo", "camisa", "clase");

# install.packages(c("rpart", "rpart.plot"));
library(rpart)
library(rpart.plot)

# Create a decision tree model
tree <- rpart(clase~genero+tipo+camisa, data=df, minsplit=2, cp=0.001);

# Visualize the decision tree with rpart.plot
fancyRpartPlot(tree);